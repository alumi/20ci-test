@extends('app')

@section('title', 'Post list')

@section('script')
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
@endsection

@section('main')
    <h1>Posts</h1>

    <p>
        <a class="btc btn-link" href="{{ route('post.create') }}">Write a post</a>
    </p>

    <ol start="{{ ($posts->currentPage()-1) * $posts->perPage() + 1 }}">
        @foreach ($posts as $post)
            <li>
                <a href="{{ route('post.show', [$post->id]) }}">{{ $post->title }}</a>

                <a class="small" data-toggle="modal" href="#quick-view" data-url="{{ route('post.show', $post->id) }}">
                    <i class="fas fa-external-link-alt"></i>
                </a>
                <a class="small" href="{{ route('post.review', $post->id) }}">
                    <i class="fas fa-edit"></i>
                </a>
            </li>
        @endforeach
    </ol>

    {{ $posts->links() }}

    <div class="modal" tabindex="-1" role="dialog" id="quick-view">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><span id="post-title"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="post-content"></div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#quick-view').on('show.bs.modal', function (e) {
            var invoker = $(e.relatedTarget);

            $.get(invoker.data('url'), function (data) {
                $('#post-content').html(data.content_rendered);
                $('#post-title').html(data.title);
            });
        });
    </script>
@endsection
