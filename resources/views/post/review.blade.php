@extends('app')

@section('title', 'Write a new post')

@section('script')
    <script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css" type="text/css">
@endsection

@section('main')
    <div class="row">
        <div class="col-12">
            <h1>Review</h1>
            <form action="{{ route('post.update', $post->id) }}" method="post">
                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        Please fix the following errors
                    </div>
                @endif

                {!! csrf_field() !!}
                <table class="table">
                    <tr>
                        <th>Title</th>
                        <td>{{ $post->title }}</td>
                    </tr>
                    <tr>
                        <th>Content</th>
                        <td>{{ $post->content }}</td>
                    </tr>
                </table>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="approved" value="1" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">
                        Approve
                    </label>
                </div>
                <br/>
                    {{ method_field('PUT') }}
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
