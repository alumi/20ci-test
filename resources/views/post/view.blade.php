@extends('app')

@section('title', $post->title)

@section('main')
    <h1>{{ $post->title }}</h1>
    <p class="text-muted small">
        <span>Posted: {{ $post->created_at->format('Y-m-d H:i:s') }}</span>
        /
        <span>Modified: {{ $post->updated_at->format('Y-m-d H:i:s') }}</span>
    </p>
    <div class="mt-3">
        @markdown($post->content)
    </div>
@endsection
