<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::get('/', 'PostController@index');

Route::get('/post/review/{id}', 'PostController@review')->name('post.review');

Route::get('/logout', function(){
    Auth::logout();

    return redirect('/');
})->name('logout');

Route::resource('post', 'PostController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
