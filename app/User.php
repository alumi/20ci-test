<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function getRoles()
    {
        return explode(',', $this->roles);
    }

    public function hasRole($roleToCheck)
    {
        foreach ($this->getRoles() as $role) {
            if ($role == $roleToCheck) {
                return true;
            }
        }

        return false;
    }
}
