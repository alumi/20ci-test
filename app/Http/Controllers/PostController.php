<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['create', 'store', 'update', 'edit']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (\Auth::check() AND $request->user()->hasRole('ADMIN')) {
            $posts = \App\Post::paginate(5);
        } else {
            $posts = \App\Post::where('is_approved', 1)->paginate(5);
        }

        return view('welcome', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.write');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title'   => 'required|max:255',
            'content' => 'required|max:4000',
        ]);

        $post = tap(new \App\Post($data))->save();

        return redirect('/');
    }

    public function update(Request $request, $id)
    {
        $post = Post::find($id);

        $post->is_approved = $request->post('approved', 0);

        $post->save();

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $post = \App\Post::where('id', $id)->where('is_approved', 1)->first();

        if (empty($post)) {
            abort(403);
        }

        if ($request->isXmlHttpRequest()) {
            $post->content_rendered = markdown($post->content);
            return response()->json($post->toArray());
        }

        return view('post.view', compact('post'));
    }

    public function review(Request $request, $id)
    {
        if (!\Auth::check() OR !$request->user()->hasRole('ADMIN')) {
            abort(403);
        }

        $post = \App\Post::find($id);

        return view('post.review', compact('post'));
    }
}
